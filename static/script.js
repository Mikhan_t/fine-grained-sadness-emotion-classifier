// CHARACTER COUNTER
document.getElementById('reddit-post').addEventListener('input', function () {
    var currentLength = this.value.length;
    var maxLength = 5000;
    var counterElement = document.getElementsByClassName('counter-char')[0];
    counterElement.textContent = currentLength + '/' + maxLength;
});


// CLEAR BUTTON
document.addEventListener("DOMContentLoaded", function () {
    var clearButton = document.getElementById("clear-btn");

    clearButton.addEventListener("click", function () {
        var textarea = document.getElementById("reddit-post");

        textarea.value = '';

        var counter = document.querySelector(".counter-char");
        counter.textContent = '0/5000';
    });
});

// EXTRACTION
document.addEventListener('DOMContentLoaded', function () {
    var extractBTN = document.getElementById('extract-btn');
    var linkTextArea = document.getElementById('reddit-post');
    var linkInput = document.getElementById('link-input');
    function updateCharacterCounter() {
        var currentLength = linkTextArea.value.length;
        var maxLength = 5000;
        var counterElement = document.getElementsByClassName('counter-char')[0];
        counterElement.textContent = currentLength + '/' + maxLength;
    }
    extractBTN.addEventListener('click', function (event) {
        event.preventDefault();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            if (xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                linkTextArea.value = data.content;
                updateCharacterCounter();
            }
        };
        xhr.send('link-input=' + encodeURIComponent(linkInput.value));
    });
});

// CHANGE TO GET LINK
document.addEventListener('DOMContentLoaded', function () {
    var textarea = document.getElementById('reddit-post');
    var linkInput = document.querySelector('.link-opt');
    var linkcont = document.getElementById('link-input');
    var linkTextArea = document.getElementById('reddit-post');
    var extractBTN = document.getElementById('extract-btn')

    var optB = document.getElementById('opt-b');
    var optA = document.getElementById('opt-a');
    var optAA = document.getElementById('opt-aa');
    optB.addEventListener('click', function () {
        textarea.style.display = 'none';
        linkInput.style.display = 'block';
        linkTextArea.style.display = 'none';
        optA.innerHTML = "Reddit Link:";
        linkTextArea.value = '';
        linkTextArea.placeholder ="";
        var counter = document.querySelector(".counter-char");
        counter.textContent = '0/5000';
        linkTextArea.value = '';
    });

    optAA.addEventListener('click', function () {
        textarea.style.display = 'block';
        linkInput.style.display = 'none';
        optA.innerHTML = "Reddit Post:";
        linkTextArea.disabled = false;
        linkTextArea.placeholder ="Enter Reddit Post";
        linkcont.value="";
    });

    extractBTN.addEventListener('click', function (){
        linkTextArea.style.display = 'block';
        linkTextArea.disabled = true;
        updateCharacterCounter()
    });
});



