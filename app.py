from flask import Flask, render_template, request, jsonify
import praw

app = Flask(__name__)

# Reddit API credentials
REDDIT_CLIENT_ID = 'a-y6I6GR5e7RyHlIKqkAXg'
REDDIT_CLIENT_SECRET = 'Ub5YorZuiggbcpiH3nYvdX7Xlk3hPQ'
REDDIT_USER_AGENT = 'post_scraper by Forehead_14'

reddit = praw.Reddit(
    client_id=REDDIT_CLIENT_ID,
    client_secret=REDDIT_CLIENT_SECRET,
    user_agent=REDDIT_USER_AGENT
)

def extract_post_data(url):
    submission = reddit.submission(url=url)
    post_title = submission.title
    post_content = submission.selftext

    return post_title, post_content

@app.route('/', methods=['GET', 'POST'])
def index():
    title, content = None, None
    if request.method == 'POST':
        reddit_post_url = request.form['link-input']
        title, content = extract_post_data(reddit_post_url)
        return jsonify({'title': title, 'content': content})
    return render_template('index.html', title=title, content=content)
if __name__ == '__main__':
    app.run(debug=True)


